# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.2.0] - 2021-12-19
### Added
- core CI jobs
- optional jobs for image scanning, performance tests, et cetera
- automated builds based on repository contents 
- various job improvements

### Changed
- file hierarchy

## [0.1.0] - 2021-12-19
### Added
- basic functionality
- basic documentation (README.md and CHANGELOG.md)

[0.2.0]: https://gitlab.com/universalci/templates/boilerplates/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/universalci/templates/boilerplates/-/tags/v0.1.0
