# universalCi v0.2.0-pre-alpha boilerplate repository

MAINTAINER alex@pyshonk.in

This repository contains templates for building gitlab ci pipeline. Feel free to use and MR.
Please note that it's a WIP and use tags as refs to fix the repo version to avoid collisions.

Supported GitLab versions are 14.5 and above.

## [0.2.0] - 2021-12-19
### Added
- core CI jobs
- optional jobs for image scanning, performance tests, et cetera
- automated builds based on repository contents 
- various job improvements

### Changed
- file hierarchy

## [0.1.0] - 2021-12-19
### Added
- basic functionality
- basic documentation (README.md and CHANGELOG.md)

[0.2.0]: https://gitlab.com/universalci/templates/boilerplates/-/tags/v0.2.0
[0.1.0]: https://gitlab.com/universalci/templates/boilerplates/-/tags/v0.1.0
